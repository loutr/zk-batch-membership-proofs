# zk-batch-membership-proofs

Variations on ZK Batch Membership Proofs.
Internship Report (M1 ENS Paris-Saclay, Sept. 2023)

## Install and build

If you're cloning this repo locally, don't forget to:
```
git submodule update --init --recursive
```
in order to pull the necessary dependencies.

In order to build the document, you can either:
- `typst compile src/main.typ <output file>`;
- `nix build .?submodules=1`.
