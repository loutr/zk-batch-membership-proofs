{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        TYPST_FONT_PATHS = "${pkgs.libertinus}/share/fonts";
      in
      {
        packages.default = pkgs.stdenv.mkDerivation {
          name = "zk-batch-membership-proofs";

          nativeBuildInputs = [ pkgs.typst ];

          src = ./.;

          inherit TYPST_FONT_PATHS;

          buildPhase = "HOME=$(mktemp -d) typst --root . compile src/main.typ";
          installPhase = "install -m 0644 -vD src/main.pdf $out/$name.pdf";

          meta = {
            description = ''
              Variations on ZK Batch Membership Proofs.
              M1 Internship Report
            '';
            homepage =
              "https://codeberg.org/loutr/zk-batch-membership-proofs";
          };
        };

        devShells = {
          default = pkgs.mkShell {
            packages = with pkgs; [ typst typst-lsp ];

            inherit TYPST_FONT_PATHS;

            # Using typst watch directly while typst-lsp doesn't work
            shellHook = ''
              export TYPST_ROOT=$(git rev-parse --show-toplevel)
              typst watch src/main.typ --open
            '';
          };

        };
      });
}

