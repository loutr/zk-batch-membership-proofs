#import "../vendor/ijtin/lib.typ": boxed
#import "@preview/xarrow:0.1.1": xarrow

// boxed environments
#let boxed = boxed(
  accent: eastern, 
  boxenv: (
    (identifier: "definition", fullname: "Definition"),
    (identifier: "theorem", fullname: "Theorem", emph: true),
    (identifier: "conjecture", fullname: "Conjecture"),
    (identifier: "assumption", fullname: "Assumption", emph: true),
  )
)

#let definition = boxed.Definition
#let assumption = boxed.Assumption
#let conjecture = boxed.Conjecture
#let theorem = boxed.Theorem

// helper functions
#let todo(body) = text(size: 1.1em, fill: red)[*[#body]*]

#let prog(header, body) = block(align(left)[
  #math.underline[#header :] \
  #pad(left: 5pt, body)
])

// operators for cryptographic primitives
#let setup = math.mono("setup")
#let accum = math.mono("accum")
#let prove = math.mono("prove")
#let verify = math.mono("verify")
#let insert = math.mono("insert")
#let comm = math.mono("comm")
#let pp = "pp"
#let acc = "acc"
#let crs = "crs"
#let negl = "negl"
#let ord = "ord"

#let cpmoda = $"cp"Pi^"modarithm"$
#let cpbound = $"cp"Pi^"bound"$

// keywords and symbols
#let ret = math.bold("return")
#let sample = {
  math.arrow.l
  move(dx: -.6em, text(size: 0.75em)[\$])
  h(-.3em)
}

// simple rewrite rules accross the document
#let show_rules(body) = {
  show "zksnark": "zkSNARK"


  set math.equation(numbering: none)
  body
}
