#import "../vendor/ijtin/lib.typ": article
#import "lib.typ": show_rules

#show: article(
  title: "Zero-Knowledge Batch Proofs for Set Accumulators:\nRefinements and Variations",

  site: "https://codeberg.org/loutr/zk-batch-membership-proofs",

  font: (
    body: "Libertinus Serif",
    mono: "Libertinus Mono",
  ),

  bibliography-file: "/refs.bib",
  
  authors: (
    (
      name: "Lucas Tabary-Maujean",
      email: "lucas.tabary@ens-paris-saclay.fr",
      organization: [ENS Paris-Saclay],
    ),
    (
      name: "Dario Fiore",
      prefix: "Supervised by",
      email: "dario.fiore@imdea.org",
      organization: [IMDEA Software Institute],
    ),
  ),

  accent: eastern,
  
  abstract: [
    This report investigates several variations and refinements of a subclass of
    Zero-Knowledge protocols for proofs on sets. More precisely, it focuses on
    solutions using RSA accumulators, which are a succinct way to commit to a
    set of values. State-of-the-art protocols using this technique have already
    been proposed in the literature and display many desirable properties,
    including succinctness. One objective is to observe these protocols in the
    light of a new computational assumption.

    In the absence of concrete results, this work explores a new possibility
    provided by this assumption and highlights where it falls short of its
    promises, after having detailed the necessary tools to understand the set of
    protocols that are being described.
  ],

  appendix: include "appendix.typ",

  single-column: true,
)

#show: show_rules

#include "00-introduction.typ"
#include "01-notions.typ"
#include "02-main_protocol.typ"
#include "03-modified_protocol.typ"
