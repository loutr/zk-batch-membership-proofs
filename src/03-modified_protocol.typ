#import "lib.typ": *
#import "@preview/xarrow:0.1.1": xarrow

= Modified protocol
#figure(
  caption: "Variant of the Batch proof membership protocol (interactive version)",
  kind: image,
  placement: top,
)[
  #let product = $limits(product)$
  #let uu = $arrow(u)$
  #let ss = $macron(s)$

  #prog($setup(lambda, "ck", pp_acc) -> crs$,
  $crs_2 <- cpmoda"."setup(lambda, "ck")
  \ crs_3 <- cpbound"."setup(lambda, "ck")
  \ ret ("ck", pp_acc, crs_2, crs_3)
  $)

  #table(
    stroke: none,
    columns: 3,
    align: (left, center + top, left),
    row-gutter: (15pt, 0pt),
    [$underline("Prover" (crs, hat(acc), c_uu; W_uu, uu,
    o_uu, S))$],
    [], [$underline("Verifier" (crs, hat(acc), c_uu))$],
    [$b_1,...,b_(2lambda) sample {0,1}
    \ s <- product_(p_i in PP_(2lambda)) p_i^b_i; space
      ss <- product_(p_i in PP_(2lambda)) p_i^(1 - b_i)
    \ hat(W)_uu <- W^ss_uu
    \ c_s <- comm_"ck" (s; o_s)
    $], [], [], [],
    [$xarrow(margin: #36pt, hat(W)_uu\, c_s)$],
    [$ell sample PP_(2^(2lambda))$],
    [$k <- s product_i u_i
    \ q ell + "res" <- k space "(Euclid)"
    \ Q <- hat(W)_uu^q 
    \ pi_2 <- cpmoda"."prove(crs_2, c_uu, c_s, h, ell, "res")
    \ pi_3 <- cpbound"."prove\(crs_3, c_uu, p_(2lambda); uu, o_uu)
    $], [$xarrow(sym: <--, margin: #48pt, ell)$], [], [],
    [$xarrow(margin: #28pt, (Q\, "res")\, pi_2\, pi_3)$],
    [$Q in^? GG_? and "res" in^? [0, ell - 1]
    \ and Q^ell hat(W)_uu^"res" eq.quest hat(acc) 
    \ and cpmoda"."verify(crs_2, c_uu, c_(s, r), h, ell, "res", pi_2)
    \ and cpbound"."verify\(crs_3, c_uu, p_(2lambda), pi_3)
    $]
  )
] <modified_protocol>

#let uu = $arrow(u)$
#let ss = $macron(s)$

This new version of the protocol starts from the observation that the original
protocol may seem redundant: there are multiple source of randomness, the one
from the $Sigma$-protocol ($r$), and the one from the randomisation process ($s,
macron(s)$). One idea is then to remove the $Sigma$-protocol construction
altogether and solely use $s, macron(s)$. The protocol then displays something
as simple as:
$
  k <- s product_i u_i \
  "res" <- k mod ell
$

Remember that the objective is to ensure the Zero-Knowledge property to our new
protocol. Here, it implies to keep the $u_i$ unseen by the verifier, while
$"res"$ is public. This can be achieved as long as $s$ (a product of randomly
chosen distinct prime numbers) is uniformly random in
$ZZ_ell^times$. Simulations agree on this, under a few conditions: $ell$ must
not be among the potential $p_i$ that $s$ is built from, but this event is
negligible because of their respective sizes; and their must be enough numbers
to populate $ZZ_ell^times$. These considerations led to this conjecture:

#conjecture(label: "conj")[
  Let $lambda, lambda'$ be security parameters and $ell in PP_(2^(2lambda))
  without P_(2lambda')$. Let ${b_i}_(i=1)^(2lambda')$ be independent Bernoulli
  variables, and $s := product_(i = 1)^(2lambda') p_i^b_i mod ell$. Then, for
  $lambda$ and $lambda'$ appropriately large, $s$ is statistically
  indistinguishable from a uniform random variable over $ZZ_ell^times$.
]

A proof of this conjecture seems out of reach for the moment. It does not seem
easy to even relate it to its most similar counterparts, which can be seen in
(recent) analytical number theory developments. @doubts assumes this conjecture
holds and presents a failed approach towards the construction of a simulator.

== Description
The protocol is shown in @modified_protocol. The central difference with
@main_protocol is the expression of $k$ as $k <- s product_i u_i$. The rest
follows: there is no more need for $h$ on the verifier side, or $r$ on the
prover side, which also removes the need to compute $R$; the final verification
is also simpler.

== Security? <doubts>
=== Simulation
The $setup$ of the RSA group is instrumented with a trapdoor in the following
way (the order of the group $ord(GG_?)$ is known because this is a modification
of the group setup): 
#align(center)[
  #prog($cal(G)'_?(lambda, N) -> pp$,
  $(GG_?, g_?; ord(GG_?)) <- cal(G)_?(lambda)
  \ ell_1, ..., ell_N sample [1, ord(GG_?)] "s.t." forall i, ell_i perp
  ord(GG_?)
  \ ell^* <- product_i ell_i
  \ ret (GG_?, g_?^(ell^*); {ell_i}, g_?)
  $)
]

The intuition for the choice of such a trapdoor is that it gives the simulator
the ability to simulate $N$ different transcripts for which it is able to compute
the $ell_i$-th roots of some appropriate group elements.

A transcript consists of the following data: $(hat(W)_uu, c_s, ell, Q,
"res", pi_2, pi_3)$. The simulator builds the $i$-th (with $1 lt.eq i
lt.eq N$) transcript with the following procedure. Using the same procedure as
the simulator of the previous protocol, it produces accepted values $pi'_2,
pi'_3, c'_s$. Then, it goes as follows:
$
  &underline(W) sample GG_?, quad hat(W)' <- underline(W)^(ell_i) \
  &"res"' sample ZZ_(ell_i), \
  &underline(hat(acc)) <- g_?^(ell^* "/" ell_i dot.op product_i u_i s^*)
   quad (underline(hat(acc))^(ell_i) = hat(acc)) \
  &Q' <- underline(hat(acc)) dot.op underline(W)^(-"res"')
$

The resulting transcript is $(hat(W)', c'_s, ell_i, Q', "res"', pi'_2, pi'_3)$.
Because $ell_i$ is coprime with the group order, the sample
$underline(W)^(ell_i)$ is uniformly random, just like $underline(W)$ is; this
makes it indistinguishable from a non-simulated version of $hat(W)_uu$, by
@randomisation_hiding. The $"res"'$ number is also indistinguishable from a
non-simulated version $"res"$, and this is a direct application of @conj. One
can then verify that $Q'^(ell_i) dot.op hat(W)'^("res"') = hat(acc)$.
Consequently, this transcript is indistinguishable from that of a successful
communication.

=== What's wrong
This construction seems to work fine from a high-level perspective, but it is
misleading. To show indistinguishability between the simulated distribution and
the honest one can be performed with the use of several hybrid distributions
@Fischlin2021. This boils the problem down to showing that the distribution of only
$(hat(W), "res")$ (honestly-produced) is indistinguishable from $(hat(W)',
"res"')$. This is however compromised by the fact that, in the honest
transcript, $"res"$ and $hat(W)$ are, as random variables, not independent (they
are both computed from $s$ and $macron(s)$, which are not independent
themselves).

The conjecture only informs us that $"res"$ is uniformly
distributed, but there is still a missing piece of information that would be
needed in order to decouple $"res"$ from $hat(W)$ in this proof.

== Adapted assumption
Resolving this kind of issue may be delicate.
One way to resolve it is to explicitly change the base assumption of the
protocol, from @randomisation_hiding to something that explicitly states that
the membership witness is indistinguishable from random, even when joined with
some partial information about its exponents.

Such an assumption could not be based on DDH-II. Even if it were to be proved in
the generic group model, it would make for a very situational assumption, which
is not very reliable.

