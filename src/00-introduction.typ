#import "lib.typ": todo, definition

= Introduction
== Original problem statement
Proofs between parties against datasets are as common on the web as it can be:
authentication for a website, for a service, and, more generally, about anything
that is being queried to a database through a client-server interaction. While
these interactions are already well known and well deployed, they may lack some
properties in specific contexts, most importantly in a distributed setup.

Say that a server wants to verify the legitimacy of a client against a set of
users (e.g. an official white list). This list is sensitive and cannot be
disclosed. It is only available as a commitment to the public.
Moreover, the list is kept by an untrusted party, that can only be trusted to
keep the correct list which corresponds to the given commitment#footnote[This is
because this commitment is typically built step by step, publicly. As long as
the one-step protocol is sound, the whole commitment is guaranteed to be valid.]. This
means that, should this list be queried, the resulting answer must be a _proof_.

The setup requires that the proof respect additional properties. Indeed,
the list remains private and no information about it should leak: the
corresponding protocols should be _Zero-Knowledge_.

Another desirable property is that the protocol scales efficiently: with our
previous example, the client should be able to ask a verification for a large
number of users at once, without compromising the efficiency of the protocol.
Let us ask that the proofs be succinct, i.e. independent of the number of users.

=== Semi-formal statement <problem_statement>
Given two sets $U, S$, a commitment (called _accumulator_, or
$"acc"$) to $S$, a security parameter $lambda$ and a set of public parameters
$"pp"$ function of $lambda$, the problem is to provide two algorithms $mono("prove")("pp", S, U) -> pi$ and
$mono("verify")("pp", "acc", U, pi) -> 0"/"1$ such that:
- the resulting protocol is complete: \ $U subset.eq S ==> mono("verify")("pp", "acc", U, mono("prove")("pp", S, U)) = 1 $;
- it is sound in an adversarial setting with polynomial complexity (also known
  as _knwoledge soundness_);
- the proofs are succinct: $|pi| = "poly"(lambda)$;
- the proofs are Zero-Knowledge (see @zk_section).

== Motivation
=== Base article
This problem has been considered several times in the literature, with the main
differences being in the technique used for set commitment. One approach for
instance, as detailed in @Tamassia2003 uses Merkle trees to provide a succinct set
representation; this method also gives logarithmic verification times in the
size of $S$.

Proofs of that kind however are neither succinct (they depend linearly on the
size of $U$, both for communication and verification time) nor Zero-Knowledge
(the global state is typically required to be public). Privacy is essentially
solved by the use of zksnarks (see @snark_section), while succinctness is
believed to be best solved by using another set commitment technique, namely RSA
accumulators (@rsa_section). 

The work described in @Fiore2021 explores this path and comes
up with a protocol which satisfies all the properties mentioned above. One
major aspect of this is the use of a computational conjecture to build a
modified set digest that is compatible with the tools used to provide anonymity.
This assumption is detailed in @randomisation_section. 

=== Variation
While this protocol, described in @main_protocol_section is satisfactory, the
present work starts from a desire to simplify it: as it will be shown, this
protocol corresponds to the intrication of two sub-protocols, a $Sigma$-protocol
and a custom randomisation procedure. Trying to combine the two in some way
looks promising, potentially reducing the concrete proof size and verification
time. Studying the relationship between these protocols may also give insights
for similar improvements for a class of protocols close to that of the base
article.

== Contributions
This work did not produce any new protocol that could be proven sound.
Regardless, this report details the different approaches taken to achieve this,
highlighting the reasons for these failures.

More precisely, it depicts what the new protocol looks like, how the security
proof was expected to work from a high level perspective, what assumptions were
needed and what potential solutions could make this work.
