#import "lib.typ": *
#import "@preview/xarrow:0.1.1": xarrow

= Original protocol <main_protocol_section>

#figure(
  caption: "Batch proof membership protocol (interactive version)",
  kind: image,
  placement: auto,
)[
  #let product = $limits(product)$
  #let uu = $arrow(u)$
  #let ss = $macron(s)$

  #prog($setup(lambda, "ck", pp_acc) -> crs$,
  $crs_2 <- cpmoda"."setup(lambda, "ck")
  \ crs_3 <- cpbound"."setup(lambda, "ck")
  \ ret ("ck", pp_acc, crs_2, crs_3)
  $)

  #table(
    stroke: none,
    columns: 3,
    align: (left, center + top, left),
    row-gutter: (15pt, 0pt),
    [$underline("Prover" (crs, hat(acc), c_uu; W_uu, uu, S))$],
    [], [$underline("Verifier" (crs, hat(acc), c_uu))$],
    [$b_1,...,b_(2lambda) sample {0,1}
    \ s <- product_(p_i in PP_(2lambda)) p_i^b_i; space
      ss <- product_(p_i in PP_(2lambda)) p_i^(1 - b_i)
    \ hat(W)_uu <- W^ss_uu
    \ r sample {0,1}^M 
    \ c_(s, r) <- comm_"ck" (s, r; o_(s, r))
    \ R <- hat(W)_uu^r
    $], [], [], [],
    [$xarrow(margin: #30pt, hat(W)_uu\, c_(s\, r)\, R)$],
    [$h sample {0, 1}^lambda
    \ ell sample PP_(2^(2lambda))
    $],
    [$k <- r + s h product_i u_i
    \ q ell + "res" <- k space "(Euclid)"
    \ Q <- hat(W)_uu^q 
    \ pi_2 <- cpmoda"."prove(crs_2, c_uu, c_(s, r), h, ell, "res")
    \ pi_3 <- cpbound"."prove\(crs_3, c_uu, p_(2lambda); uu)
    $], [$xarrow(sym: <--, margin: #45pt, h\, ell)$], [], [],
    [$xarrow(margin: #28pt, (Q\, "res")\, pi_2\, pi_3)$],
    [$Q in^? GG_? and "res" in^? [0, ell - 1]
    \ and Q^ell hat(W)_uu^"res" eq.quest hat(acc)^h R 
    \ and cpmoda"."verify(crs_2, c_uu, c_(s, r), h, ell, "res", pi_2)
    \ and cpbound"."verify\(crs_3, c_uu, p_(2lambda), pi_3)
    $]
  )
] <main_protocol>

#let uu = $arrow(u)$
#let ss = $macron(s)$

== Description
The original protocol is presented in @main_protocol. For clarity purposes, this
corresponds to the interactive version. The non-interactive protocol, closer to the
actual implementation of the protocol, is given in
@main_protocol_non-interactive. 

The protocol #setup generates a common reference string (or CRS) for all
parties. This CRS is the concatenation of the public parameters and
keys needed for the different objects: the commitment scheme, the accumulator,
the two CP-SNARKS.

The prover knows the set $S$, the subset (represented as a vector for
convenience) $uu$, the corresponding RSA accumulator witness $W_uu$. The
verifier only has access to public information: the public accumulator to $S$
(from which it can easily derive $hat(acc) = accum(S union PP_(2lambda))$, which
is the one of interest here) and a commitment to $uu$.

As for the protocol itself, the prover starts by sampling the bits $b_i$ in
order to choose which elements to add, following the randomisation method of the
witness $W_uu$ described before @randomisation_section. It actually computes the
product of the chosen elements (say $macron(s)$), used for the randomisation
itself, and the product of the remaining elements ($s$), which is used in the
verification process. Then it samples a uniformly random number $r$, which
serves later as the hiding factor (this follows a standard pattern in
$Sigma$-protocols @Damgaard2002, and can be seen in the classic example of
Schnorr's signature). This number, as well as $s$, are sent over in the form of
a commitment. The prover also sends $R = W_uu^r$, which is needed later on for
the verifier.

The verifier responds with two challenges: $h$ and $ell$. These two challenges
are needed because of the use of different subprotocols: $h$ is related to the
$Sigma$-protocol itself, and $ell$ is used to obtain extraction even in the case
of the PoKE used later on by the prover.

Then, the prover could transmit $k$ directly, but then the proof would no longer
be succinct, as $|k| = O(|uu|)$. Instead, a PoKE of $k$ is sent as $(Q, "res")$,
and can then be used by the verifier. The CP-SNARK $cpmoda$ is used to ensure
that indeed $"res" = r + s h product_i u_i mod ell$, as this necessary equality
cannot be deduced from the rest of the protocol.

== Security
As stated before, this protocol has all the desired properties listed in
@problem_statement. Here follows a quick overview of the corresponding security
proofs.

=== Extraction
The extraction property of the protocol is detailed in @Fiore2021. In some
sense, the variant studied later reveals strictly more information than this
one, and the extraction property that goes with it is thus strictly simpler. 

=== Simulation
This protocol is fundamentally a $Sigma$-protocol, which almost gives the
statistical Zero-Knowledge property. The simulator works roughly as follows:
- it uses the simulator from the different CP-SNARKS to produce simulated
  proofs $pi'_3, pi'_2$;
- it samples $ell' sample PP_(2lambda)$ and $h sample {0, 1}^lambda$;
- it samples $"res"' sample [0, ell - 1]$ (#math.circle.filled);
- it samples $Q' sample GG_?$ (#math.square.filled);
- it samples $hat(W)'_uu sample GG_?$ (#math.square.filled);
- it computes $R' <- Q'^ell hat(W)_uu^("res"') hat(acc)^(-h)$
  (#math.circle.filled).
The corresponding transcript can then be shown to be indistinguishable from an
honest transcript, thanks to the DDH-II assumption (#math.square.filled) or
standard, statistical indistinguishability results (#math.circle.filled).
