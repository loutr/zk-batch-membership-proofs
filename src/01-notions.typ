#import "lib.typ": *

= Notions
This section describes the essential building blocks of the considered protocol, as
well as the concepts necessary to describe its associated proofs.

=== Computational model
A conventional model to prove security or other properties in a cryptographic
setting is that of a probabilistic, polynomial time adversary (PPT). Most
assumptions then have a similar shape, generally described as an
indistinguishability game: no PPT adversary can guess the difference between two
versions of some object, except with negligible probability #footnote[In a
cryptographic context, with a security parameter $lambda$, a probability
$p$ (seen as a function of $lambda$) is negligible if, for any integer $k$,
$p(lambda) = O(1 "/" lambda^k)$.], which can be denoted $negl(lambda)$.

A algorithm is _efficient_ if it runs in polynomial time. From there, two
probability distributions are _indistinguishable_ if no efficient algorithm can
distinguish between the two.


== RSA Accumulators <rsa_section>
Accumulators are commitment to sets of elements. Their precise definition
actually involves multisets, using conventional notations: $S_1 union.plus S_2$
for the multiset union of two multisets $S_1$ and $S_2$, and $S_1 subset S_2$
for their multiset inclusion. Such a construct consists of these primitives:
- $setup(lambda) -> pp$, outputs public parameters for a given security
  parameter. This includes the accumulator for the empty set, $bold(1)$;
- $insert(pp, acc, x) -> acc'$ computes an accumulator for $S union.plus {x}$,
  if $acc$ is an accumulator for $S$. Using $bold(1)$, and this program, it is
  easy to build a program $accum(S) -> acc$ that builds an accumulator for a
  given multiset;
- $prove(pp, S, X) -> W_X$ output a _batch_ membership proof (i.e. that
  $X subset S$), this proof must be succinct (in particular, independent of
  the size of $X$);
- $verify(pp, acc, X, W_X) -> 0"/"1$ accepts or rejects a membership proof $W_X$;

The accumulator may also support other operations, like non membership proofs.
Please note that, although similar, these primitives do not provide valid
answers to the original problem stated in @problem_statement; specifically
because nothing is said about the privacy of $S$ within the $prove$ program.

#definition(name: "Accumulator Security", label: "acc_security")[
  An accumulator is secure if any PPT adversary $cal(A)$ has negligible
  probability to output a triplet $(X, S, W)$ such that $X subset.not S$ and $W$
  is a membership proof such that $verify(pp, accum(S), X, W) = 1$.
]

=== Specificities
RSA accumulators @Bari1997 are accumulators to sets of integers.
These accumulators are secure in the sense of @acc_security, as long as only
prime numbers are allowed in the set @Bari1997. The intuition for the
underlying proof is that the accumulator built to represent a composite number
is the same as the one built to represent the multiset of its divisors: $g_?^6 =
g_?^(2 times 3)$, which allows the adversary to produce $g_?^3$, a correct
witnes of ${2} subset {6}$. When only primes are allowed, prime decomposition
and the Strong RSA assumption #cite("Bari1997", "Boneh2018") give the result.

=== Implementation
The implementation is detailed in @rsa_acc. The #setup algorithm simply uses a
generator of a group of hidden order (in our case, an RSA group)
which returns both a description of the group and a generator. #insert behaves
as expected --- it is also clear that an implementation of #accum would be to
return $g_?^(pi_S)$, where $pi_S = product_(x in S) x$. 

The #prove algorithm essentially returns, if the set $S$ has an accumulator
representation $acc$, a $pi_X$-th root of $acc$, where $pi_X$ is the product of
the elements of $X$. The #verify algorithm verifies that $W_X$ is indeed a
correct root.

#figure(caption: "Primitives of the RSA accumulator")[
  #grid(columns: 2, gutter: 15pt,
    prog($setup(lambda) -> pp$,
      $(GG_?, g_?) <- cal(G)_?(lambda)\
      ret (GG_?, g_?) 
      $
    ),
    prog($insert(pp, acc, x) -> acc'$,
      $ret acc^x$
    ),
    prog($prove(pp, S, X) -> W_X$,
      $pi_S <- product_(x in S) x \
       pi_X <- product_(x in X) x \
       "(" pi_X | pi_S "because" X subset S ")" \
       ret g_?^(pi_S "/" pi_X)
      $
    ),
    prog($verify(pp, acc, X, W_X) -> 0 "/" 1$,
      $pi_X <- product_(x in X) x \
       ret W_X^(pi_X) eq.quest acc
      $
    ),
  )
] <rsa_acc>

=== Practical considerations
These accumulators are restricted to sets with elements in $PP$, the set of all
prime numbers. This can easily be generalised to any set $D$ such that every
pair of elements are coprime.

This is exactly what division-intractable hash functions @Coron2000 produce:
different inputs give different outputs, coprime with each other --- with high
probability. It is possible to build such functions in an efficient manner,
under plausible conjectures @Ozdemir2019.

This enables to represent a wider variety of set elements. In what follows, and
without loss of generality, any set associated to an accumulator is considered
to have prime numbers as elements. These elements can also be considered all
larger than a given constant.

=== Randomisation <randomisation_section>
Accumulators constitute the backbone of the protocol of interest here, and the
transmission of a witness to the verifier is necessary. In specific cases, if
transmitted as-is, it might be subject to brute-force attacks @Fiore2021. More
generally, to ensure the Zero-Knowledge property of the protocol, it is
important to make it indistinguishable from a random group element.

The approach is to add randomly-chosen (prime) elements to the set, all smaller
than any real set element. Assuming that all elements of $S$ are larger than
$p_(2lambda)$, the $2lambda$-th prime; given a membership witness $W$, a
uniformly random subset of $PP_(2lambda) = { p_1, ..., p_(2lambda) }$ is chosen:
$b_1, ..., b_(2lambda) sample {0, 1}$. The randomised witness $hat(W)$ is then built by
inserting in $W$ (which can be seen as an accumulator) every $p_i$ such that
$b_i = 1$. More directly: $hat(W) <- W^s$, where $s = product_i p_i^b_i$.

== Hardness assumptions
The main hardness assumption used by the original protocol is denoted DDH-II. It
is a variant of the original Differential Diffie-Hellman assumption (both
described in @Canetti1997). This assumption is valid in the generic group
model @Shoup1997; this makes it more plausible. Under DDH-II, the following
result is true:

#theorem(name: "Randomisation is hiding", label: "randomisation_hiding")[
  For any parameters $pp <- setup(lambda)$, set of prime numbers $S$ (where $S
  sect PP_(2lambda) = emptyset$), $R sample GG_?$ and $hat(W)$ computed from the
  randomisation method described above, it holds that:
  $
    | Pr[cal(A)(pp, S, hat(W)) = 0] - Pr[cal(A)(pp, S, R) = 0] | = negl(lambda)
  $
]

== Zero-Knowledge <zk_section>
This section has no pretention to rigourously define what a Zero-Knowledge
protocol is, as this is a more subtle task than it seems. Please refer to
@Thaler2022 for a detailed description. The following assumes an
understanding of what _interactive proofs_ are.

#definition(name: "Proof system (informal)")[
  A proof system for a given langage $cal(L)$ is an interactive proof for which
  there exist an efficient algorithm $cal(E)$, called the _extractor_, able to
  interact repeatedly with the prover $cal(P)$, that can, for an input $x in
  cal(L)$, output a witness $w$ of that statement.
]

#definition(name: "Zero-Knowledge protocol (informal)")[
  A proof system for a language $cal(L)$ is said to be (honest-verifier) Zero-Knowledge if there exist
  an efficient algorithm $cal(S)$, called the _simulator_, that, for any $x in
  cal(L)$, outputs a transcript of the interactive proof whose distribution is
  _close_ to that of the original system.
  - if _close_ means identical, the protocol is described as _perfect
    Zero-Knowledge_;
  - if _close_ means statistically close, the protocol is _statistically
    Zero-Knowledge_;
  - if _close_ means indistinguishable, the protocol is _computationally
    Zero-Knowledge_.
]

== SNARKs <snark_section>
A SNARK is a succinct non-interactive argument of knowledge. More precisely, a
SNARK $Pi$ is a triplet of three algorithms $Pi\.setup, Pi\.prove, Pi\.verify$
that verifies specific properties, as listed in @Fiore2021.

=== Commit and prove
Commit-and-Prove SNARKs (or CP-SNARKs) are SNARKs that can produce proofs for an
element, using only a commitment to that element. These schemes can be used to
build @Fiore2019 zkSNARKs, that is, SNARKs whose proofs reveal nothing
about their given input. This is the construction that matters for the rest of
this report, and it is essentially treated in a blackbox way.

=== Use in the protocol and limitations.
The protocols make use of zkSNARKs to ensure certain properties:
- a first argument system, called $cpmoda$, ensures to the verifier that a
  relationship of the following form holds: $"res" = r + s h product_i u_i mod
  ell$, where $r, s$ and $arrow(u)$ are only known to the verifier via
  commitments.
- a second argument system, called $cpbound$, ensures to the verifier that all
  given numbers in $arrow(u)$ (only provided as a commitment) are larger than a
  fixed $p_(2lambda)$. Using the same notations as before, this is the argument
  used to ensure the randomisation detailed above is applied on a set with
  appropriately large elements.

It is important to note that, albeit succinct, the concrete size of the SNARK
proofs depends on which constraints were encoded in it. For instance, the two
systems right above are described with simple arithmetic and existential
quantifiers (both for the modulo and the ordering). This results in something
much more lightweight than the naive (but sound!) approach to encode RSA
computations, which would make the overall protocol look simpler.

#figure(
  caption: "Interactive version of the PoKE",
  kind: image,
)[
  #let product = $limits(product)$
  #let uu = $arrow(u)$
  #let ss = $macron(s)$

  #prog($setup(lambda) -> crs$,
  $(GG_?, g_?) <- cal(G)_?(lambda)
  \ ret (GG_?)
  $)

  #table(
    stroke: none,
    columns: 3,
    align: (left, center + top, left),
    row-gutter: (15pt, 0pt),
    [$underline("Prover" (crs, A, B; x))$],
    [], [$underline("Verifier" (crs, A, B))$],
    [], [], [$ell sample PP_(2^(2lambda))$],
    [$q ell + "res" <- x space "(Euclid)"
    \ Q <- A^q 
    $], [$xarrow(sym: <--, margin: #40pt, ell)$], [], [],
    [$xarrow(margin: #28pt, (Q\, "res"))$],
    [$Q in^? GG_? and "res" in^? [0, ell - 1]
    \ and Q^ell A^"res" eq.quest B
    $]
  )
] <poke>

== PoKE
PoKEs are small protocols introduced in @Boneh2018 used to build succinct
proofs, in a given group of unknown order $GG_?$ of statements of the form:
$exists x, A^x = B$. The interactive version of the protocol is depicted in
@poke. 

