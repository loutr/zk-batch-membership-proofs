#import "lib.typ": *

= Original protocol -- non-interactive version <main_protocol_non-interactive>
  
#figure(caption: "non-interactive verion of the Batch membership protocol")[
  #let ss = $macron(s)$
  #let uu = $arrow(u)$
  #let div = $||$

  #stack(dir: ltr,
    prog($prove\(crs, acc, c_uu; W_uu, uu) -> pi$,
      $"let" u^* = product_i u_i, space p^* = product_(p_i in PP_(2lambda)) p_i
     \ hat(acc) <- acc^(p^*)
      \ b_1,...,b_(2lambda) sample {0,1}
    \ s <- product_(p_i in PP_(2lambda)) p_i^b_i; space
      ss <- product_(p_i in PP_(2lambda)) p_i^(1 - b_i)
    \ hat(W)_uu <- W^ss_uu
    \ r sample {0,1}^M 
    \ c_(s, r) <- comm_"ck" (s, r; o_(s, r))
    \ R <- hat(W)_uu^r
    \ h <- H(crs div acc div c_uu div c_(s, r) div hat(W)_uu div R)_1
    \ ell <- H_("prime")(crs div acc div c_uu div c_(s, r) div hat(W)_uu div R)_2
    \ k <- r + s h product_i u_i
    \ q ell + "res" <- k space "(Euclid)"
    \ Q <- hat(W)_uu^q 
    \ pi_2 <- cpmoda"."prove(crs_2, c_uu, c_(s, r), h, ell, "res")
    \ pi_3 <- cpbound"."prove\(crs_3, c_uu, p_(2lambda); uu)
    \ ret (hat(W)_uu, R, c_(s, r), pi_1, pi_2, Q, "res")
    $),
    h(15pt),
    prog($verify(crs, acc, c_uu, pi) -> 0"/"1$,
      $hat(acc) <- acc^(p^*)
      \ "parse" pi "as" (hat(W)_uu, R, c_(s, r), pi_1, pi_2, Q, "res")
      \ h <- H(crs div acc div c_uu div c_(s, r) div hat(W)_uu div R)_1
      \ ell <- H_("prime")(crs div acc div c_uu div c_(s, r) div hat(W)_uu div R)_2
      \ ret Q in^? GG_? and "res" in^? [0, ell - 1]
      \ and Q^ell hat(W)_uu^"res" eq.quest hat(acc)^h R 
      \ and cpmoda"."verify(crs_2, c_uu, c_(s, r), h, ell, "res", pi_2)
      \ and cpbound"."verify\(crs_3, c_uu, p_(2lambda), pi_3)
      $
    ),
  )
] <non-interactive>

@non-interactive presents the original protocol in a non-interactive way. The
transformation from the interactive description is achieved via a Fiat-Shamir
transform, which is a sound transformation in the random oracle model.

= Acknowledgements and observations
The internship on which this report is based took place over 5 months at IMDEA
Software Institute, near Madrid. I would like to thank everyone I met there for
their kindness and the time they gave me; in particular my supervisor Dario
Fiore, as well as Dimitris Kolonelos, with whom I shared my office and worked
the most with on the very topic of my internship.

I have had the chance to discover many new interesting subjects during that
time, mostly thanks to the weekly reading group sessions that were organised by
the crypto team.

These months also witnessed several important personal experiences, including my
first paper presentation at a conference in San Francisco, on the
article I had the pleasure and chance to co-author last year for my L3
internship, which took up a good part of my month of June.

On a more nuanced level, I had chosen this internship in order to discover this
field that I only knew on the surface, and it helped me realise that it is not
really for me. Likewise, over the last few months I have had the opportunity to
question my relationship with research and its professions, and as a result I
have decided to branch more toward teaching, even though I had been considering
research as my only option for a long time. I feel satisfied by this decision.
However, I believe all of this has had an impact on my motivation and the
overall quality of the work I produced. Far from being an excuse, I believe it
is still important to contextualise this work and share these personal thoughts.
